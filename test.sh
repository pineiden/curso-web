project_name=$(pwd|awk -F'/' '{print $NF}')
templates=1
static=1
media=0
urls=1

while getopts 'ntsmui:' option
do
case "${option}"
in
  n) app=${OPTARG};;
  t) templates=${OPTARG};;
  s) static=${OPTARG};;
  m) media=${OPTARG};;
  u) urls=${OPTARG};;
  i) project_name=${OPTARG};;
esac
done

echo $project_name
echo $urls
echo "Creando apps en ${project_name}"
