from django.db import models
from django.utils.text import slugify
import json


def get_upload_pagina_file_name(instance, filename):
    return "paginas/%s/%s" % (instance.slug_titulo, filename)

# Create your models here.


class Clasificacion(models.Model):
    tipo = models.CharField(
        max_length=20,
        unique=True)
    slug_tipo = models.SlugField(unique=True)
    descripcion = models.TextField(blank=True)

    def get_absolute_url(self):
        return reverse('pagina:clasificacion',
                       kwargs={'slug_tipo': self.slug_tipo})

    def save(self, *args, **kwargs):
        self.slug_tipo = slugify(self.tipo)
        # TODO: check if exists before save
        super(Clasificacion, self).save(*args, **kwargs)

    @property
    def Clasificacion(self):
        return self.tipo

    class Meta:
        app_label = "pagina"
        verbose_name = "Clasificación"
        verbose_name_plural = "Clasificaciones"
        ordering = ("tipo", "fecha_publicacion")

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.tipo


class Pagina(models.Model):
    titulo = models.CharField(max_length=100)
    slug_titulo = models.SlugField()
    imagen = models.ImageField(
        upload_to=get_upload_pagina_file_name,
        blank=True)
    fecha_publicacion = models.DateTimeField(auto_now=True)
    resumen = models.TextField(default="Escribe un resumen")
    cuerpo = models.TextField(help_text="Escribe todo el contenido")
    clasificacion = models.ForeignKey(
        Clasificacion,
        on_delete=models.CASCADE,
        blank=True,
        null=True)

    def get_absolute_url(self):
        return reverse('pagina:detalle_pagina',
                       kwargs={'slug': self.slug_titulo})

    def save(self, *args, **kwargs):
        self.slug_titulo = slugify(self.titulo)
        # TODO: check if exists before save
        super(Pagina, self).save(*args, **kwargs)

    @property
    def Pagina(self):
        return self.titulo

    class Meta:
        app_label = "pagina"
        verbose_name = "Página"
        verbose_name_plural = "Páginas"
        ordering = ("clasificacion", "fecha_publicacion")

    def __str__(self):
        return self.titulo

    def __repr__(self):
        return json.dumps(self.__dict__)
