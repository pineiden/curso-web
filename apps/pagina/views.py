from django.shortcuts import render
from django.views.generic import (DetailView,
                                  ListView,
                                  CreateView,
                                  UpdateView,
                                  DeleteView)
from .models import (Clasificacion, Pagina)
from django.urls import reverse, reverse_lazy

# Create your views here.

class VistaPagina(DetailView):
    model =  Pagina
    slug_field = 'slug_titulo'
    template_name = 'pagina/pagina.dj.html'


class ListarPaginas(ListView):
    model = Pagina
    template_name = 'pagina/lista_paginas.dj.html'
    allow_empty = True
    paginate_by = 20

class CrearPagina(CreateView):
    model = Pagina
    template_name = 'pagina/edita_pagina.dj.html'
    slug_field = 'slug_titulo'
    sucess_url = reverse_lazy(
        'edicion_exitosa/',
        args=(self.object.id))

class EditaPagina(UpdateView):
    model = Pagina
    template_name = 'pagina/edita_pagina.dj.html'
    slug_field = 'slug_titulo'
    sucess_url = reverse_lazy(
        'edicion_exitosa/',
        args=(self.object.id))

class EliminaPagina(DeleteView):
    model = Pagina
    template_name = 'pagina/edita_pagina.dj.html'
    slug_field = 'slug_titulo'
    sucess_url = reverse_lazy(
        'eliminacion_exitosa/',
        args=(self.object.id))

class EdicionPaginaExitosa(VistaPagina):
    template_name = 'pagina/edicion_pagina_exitosa.dj.html'

class EliminacionPaginaExitosa(VistaPagina):
    template_name = 'pagina/eliminacion_pagina_exitosa.dj.html'
