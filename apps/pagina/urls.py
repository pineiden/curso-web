from django.conf.urls import url
from django.urls import path
# from django.contrib import admin
from apps.pagina import (VistaPagina,
                         ListarPaginas,
                         CrearPagina,
                         EditaPagina,
                         EdicionPaginaExitosa,
                         EliminacionPaginaExitosa,
                         EliminaPagina)

app_name='pagina'

urlpatterns = [
    path('<slug:slug_titulo>',
         VistaPagina.as_view(),
         name='detalle_pagina'),
    path('lista/',
         ListarPagina.as_view(),
         name='lista_paginas'),
    path('crear/',
         CrearPagina.as_view(),
         name='crea_pagina'),
    path('edita/<int:id>',
         EditaPagina.as_view(),
         name='edita_pagina'),
    path('elimina/<int:id>',
         EliminaPagina.as_view(),
         name='elimina_pagina'),
    path('edicion_exitosa/<int:id>',
         EdicionPaginaExitosa.as_view(),
         name='edicion_pagina_exitosa'),
    path('eliminacion_exitosa/<int:id>',
         EliminacionPaginaExitosa.as_view(),
         name='eliminacion_pagina_exitosa')
    ]
