"""
WSGI config for mi_web project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from django_herramientas.ambiente import get_env_variable

STAGE=get_env_variable('STAGE')
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'mi_web.settings.%s' %STAGE)

application = get_wsgi_application()
