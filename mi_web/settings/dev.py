from .base import *

DEBUG = True

buscar = 'django.contrib.staticfiles'
index = INSTALLED_APPS.index(buscar)
INSTALLED_APPS.insert(index, 'livereload')
MIDDLEWARE.append('livereload.middleware.LiveReloadScript')


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

SITE_URL="http://www.miweb.cl"
SITE_NAME="Mi Web"
GOOGLE_API_KEY=''
SITE_ID = 1
ALLOWED_HOSTS=["*"]
