CONTACTO = {
    'telefono':"+56982142267",
    'facebook':None,
    'twitter':None,
    'instagram':None,
    'linkedin':None,
    'direccion': 'Arturo Prat 33',
    'comuna': 'Santiago'
}

SITIO = {
    'nombre':'Registro de Zonas de Sacrificio',
    'frase':'¡Por la Salud de los Pueblos!',
    'web':'Zonas de Sacrificio'
}

import json

class TextoLink:
    def __init__(self, texto, link='/'):
        self.texto=texto
        self.link=link
    def __repr__(self):
        return json.dumps(self.__dict__)

NAV_MENU = [
    ('home', TextoLink('Inicio')),
    ('nosotres', TextoLink('Nosotres',link='/nosotres')),
    ('proyecto', TextoLink('El Proyecto', link='/proyecto')),
    ('contacto', TextoLink('Contacto', link='/contacto')),
]


NV = 'seven'
