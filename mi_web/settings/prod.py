import os
from .base import *

DEBUG = False

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DBNAME = 'misdatos'
DBUSER = 'usuario'
DBPASS = 'mipassword'

engine='django.db.backends.postgresql_psycopg2'

DATABASES = {
    'default': {
         'ENGINE': engine,
         'NAME': DBNAME,
         'USER': DBUSER,
         'PASSWORD':DBPASS,
         'HOST': '',
         'PORT': '',
    },
}


SITE_URL="http://www.miweb.cl"
SITE_NAME="Mi Web"
ALLOWED_HOSTS=['localhost',
               'miweb.cl',]
SITE_ID = 1
