#!/usr/bin/env python
import os
import sys
from django_herramientas.ambiente import get_env_variable

if __name__ == '__main__':
    STAGE=get_env_variable('STAGE')
    os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                          'mi_web.settings.%s' %STAGE)
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
